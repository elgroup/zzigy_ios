//
//  WebServiceSessionHelper.swift
//  MySwiftExample
//
//  Created by anupama on 17/01/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit
import Foundation
var URI_HTTP = String(format: "http://45.55.131.66/liquirejet/api")

@objc protocol WebServiceSessionHelperDelegate
{
    func gettingFail(error: String)
    
    @objc optional func loginSuccess(response: String)
    @objc optional func changePassword(response: String)
    @objc optional func registration(response: String)
    @objc optional func forgotPassword(response: String)
    @objc optional func registrationWithPic(response: String)
    @objc optional func showUserProfile(response: String)
    @objc optional func editProfile(response: String)
    @objc optional func showFavourite(response: String)
    @objc optional func addFavourite(response: String)
    @objc optional func deleteFavourite(response: String)
    @objc optional func uploadPicture(response: String)
    @objc optional func uploadStory(response: String)
    @objc optional func uploadMessage(response: String)
    @objc optional func downloadStory(response: String)
    
    @objc optional func post(response: String)
    @objc optional func post1(response: String)
    @objc optional func post2(response: String)
    @objc optional func post3(response: String)
    
   
    @objc optional func insertMLSID(response: String)
    @objc optional func deleteMLSID(response: String)
    @objc optional func profileEditing(response: String)
    @objc optional func notesUpdating(response: String)
    @objc optional func notesDeleting(response: String)
    @objc optional func historyLog(response: String)
    @objc optional func realtorShow(response: String)
    @objc optional func notesgetting(response: String)
    @objc optional func updateStatus(response: String)
    @objc optional func callDelete(response: String)
    @objc optional func postphp(response: String)
}

class WebServiceSessionHelper : NSObject, URLSessionDataDelegate, URLSessionDelegate, URLSessionTaskDelegate
{
    var serviceName : String! = nil
    var responseString :String! = nil
    weak var  delegate: WebServiceSessionHelperDelegate?
    
    // MARK:...........................................................
    // MARK:...Request With Post.Url and Paramter in String..................
    func jsonPaseringWithPHP(datastring: String, urlString: String)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        let url = URL(string: urlString)!
        let postData = datastring.data(using: String.Encoding.ascii) as Data!
        let postLength =  String(format: "%lu", (postData?.count)!)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = postData 
        let dataTask = defaultSession.dataTask(with: request as URLRequest)
        dataTask.resume()
        
    }
    // MARK:...........................................................
    // MARK:...Request With Url .................
    func requestWithURL(urlString: String)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let  defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        let url = URL(string: urlString)!
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let dataTask = defaultSession.dataTask(with: request as URLRequest)
        dataTask.resume()
        
    }
    // MARK:...........................................................
    // MARK:...Request With Url and Data..................
    func requestWithURLPic(urlString: String, picData: NSData)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let   defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        let url = URL(string: urlString)!
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue(String(format: "%lu", (picData.length)), forHTTPHeaderField: "Content-Length")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let dataTask = defaultSession.dataTask(with: request as URLRequest)
        dataTask.resume()
        
    }
    // MARK:...........................................................
    // MARK:...Request With Post.Url and Dictionary..................
    func requestWithPost(urlString: String, Dict: NSDictionary)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: Dict, options: .prettyPrinted) as Data!
            // here "jsonData" is the dictionary encoded in JSON data
            
            let url = URL(string: urlString)!
            let postRequest = NSMutableURLRequest(url: url)
            postRequest.httpMethod = "POST"
            postRequest.setValue(String(format: "%lu", (jsonData?.count)!), forHTTPHeaderField: "Content-Length")
            postRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            postRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            postRequest.httpBody = jsonData
            let dataTask = defaultSession.dataTask(with: postRequest as URLRequest)
            dataTask.resume()
        }
        catch
        {
            print(error.localizedDescription)
        }
    }
    
    // MARK:...........................................................
    // MARK:...Post Image and Parameter On PHPserver...................
    func postImageandParameterOnPHPserver(urlString: String, uploadImage: String,  parameters: Dictionary<String,AnyObject>?)
    {
        let defaultConfigObject = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: defaultConfigObject,  delegate: self,  delegateQueue: OperationQueue.main)
        let myImage = UIImage.init(named: uploadImage)
        let imageData:NSData = UIImagePNGRepresentation(myImage!)! as NSData

        let url = URL(string: urlString)!
        let postRequest = NSMutableURLRequest(url: url)
        postRequest.httpMethod = "POST"
        let boundary:String = "---------------------------14737809831466499882746641449"
        let contentType:String = "multipart/form-data; boundary=%@"
        postRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        let mybodyData:NSMutableData = NSMutableData();
        mybodyData.append(String(format: "\r\n--%@\r\n", boundary).data(using: String.Encoding.utf8)!)
        mybodyData.append(String(format: "Content-Disposition: form-data; name=\"userfile\"; filename=\"ipodfile.jpg\"\r\n", boundary).data(using: String.Encoding.utf8)!)
        mybodyData.append(String(format: "Content-Type: application/octet-stream\r\n\r\n", boundary).data(using: String.Encoding.utf8)!)
        mybodyData.append(imageData as Data)
        mybodyData.append(String(format: "\r\n--%@--\r\n", boundary).data(using: String.Encoding.utf8)!)

        if parameters != nil
        {
            for (key, value) in parameters!
            {
               mybodyData.append(String(format: "--%@\r\n", boundary).data(using: String.Encoding.utf8)!)
                mybodyData.append(String(format: "Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key).data(using: String.Encoding.utf8)!)
                mybodyData.append(String(format: "%@", value as! CVarArg).data(using: String.Encoding.utf8)!)
                mybodyData.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        mybodyData.append(String(format: "--%@--\r\n", boundary).data(using: String.Encoding.utf8)!)
        postRequest.httpBody = mybodyData as Data
        
        let dataTask = defaultSession.dataTask(with: postRequest as URLRequest)
        dataTask.resume()
    
    }
    
    func login(param : String)
    {
        serviceName="Login"
        let postString = String(format: "%@%@", URI_HTTP,param)
        requestWithURL(urlString: postString)
    }
    func changePassword(urlString : String)
    {
        serviceName="ChangePassword"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func registration(urlString : String)
    {
        serviceName="Registration"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func registrationWithPic(urlString : String)
    {
        serviceName="RegistrationPic"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func forgotPassword(urlString : String)
    {
        serviceName="ForgotPassword"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func showUserProfile(urlString : String)
    {
        serviceName="ShowUserPfrofile"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func editProfile(urlString : String)
    {
        serviceName="EditPfrofile"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func showFavourite(urlString : String)
    {
        serviceName="ShowFavourite"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func addFavourite(urlString : String)
    {
        serviceName="AddFavourite"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func deleteFavourite(urlString : String)
    {
        serviceName="DeleteFavourite"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func uploadPicture(urlString : String)
    {
        serviceName="UploadPicture"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func uploadStory(urlString : String)
    {
        serviceName="UploadStory"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func uploadMessage(urlString : String)
    {
        serviceName="UploadMessage"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    func downloadStory(urlString : String)
    {
        serviceName="DownloadStory"
        let postString = String(format: "%@%@", URI_HTTP,urlString)
        requestWithURL(urlString: postString)
    }
    
    // MARK:  Post method...................
    func postphp(urlString : String, uploadImage : String, params : NSDictionary)
    {
        serviceName="Postphp"
        postImageandParameterOnPHPserver(urlString: urlString, uploadImage: uploadImage, parameters: params as? Dictionary<String, AnyObject>)
    }
    
    func post1(urlString : String, params : String)
    {
        serviceName="Post1"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func post2(urlString : String, data : NSData)
    {
        serviceName="Post2"
        requestWithURLPic(urlString: urlString, picData: data)
    }
    func post3(urlString : String, params : String)
    {
        serviceName="Post3"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func callDelete(urlString : String, params : String)
    {
        serviceName="Post4"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func insertMLSID(urlString : String, params : String)
    {
        serviceName="Post5"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func deleteMLSID(urlString : String, params : String)
    {
        serviceName="Post6"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func profileEditing(urlString : String, params : String)
    {
        serviceName="Post7"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func notesUpdating(urlString : String, params : String)
    {
        serviceName="Post8"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func notesDeleting(urlString : String, params : String)
    {
        serviceName="Post9"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
     func historyLog(urlString : String, params : String)
    {
        serviceName="Post10"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func realtorShow(urlString : String, params : String)
    {
        serviceName="Post11"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func notesgetting(urlString : String, params : String)
    {
        serviceName="Post12"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    func updateStatus(urlString : String, params : String)
    {
        serviceName="Post13"
        jsonPaseringWithPHP(datastring: params, urlString: urlString)
    }
    
    // MARK:- URLSessionDownloadDelegate
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data)
    {
        print("Did receive data!",data)
        URLSessionDidFinishLoading(reciveData: data)
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?)
    {
       
        if (error != nil) {
            print("didCompleteWithError \(error?.localizedDescription)")
            delegate?.gettingFail(error: (error?.localizedDescription)! )
        }
        else {
            print("The task finished successfully")
        }
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void)
    {
        print("The task finished successfully",response)
        completionHandler(.allow)
    }
    // MARK:...........................................................
    // MARK:...Url Session Finish method...................
    func URLSessionDidFinishLoading(reciveData :Data)
    {
        responseString = String(data: reciveData, encoding: String.Encoding.utf8)  as String!
        if serviceName.isEqual("Login")
        {
            delegate?.loginSuccess!(response: responseString)
        }
        else if serviceName.isEqual("ChangePassword")
        {
            delegate?.changePassword!(response: responseString)
        }
        else if serviceName.isEqual("Registration")
        {
            delegate?.registration!(response: responseString )
        }
        else if serviceName.isEqual("ForgotPassword")
        {
            self.delegate?.forgotPassword!(response: responseString )
        }
        else if serviceName.isEqual("ShowUserPfrofile")
        {
            delegate?.showUserProfile!(response: responseString )
        }
        else if serviceName.isEqual("EditPfrofile")
        {
            delegate?.editProfile!(response: responseString )
        }
        else if serviceName.isEqual("ShowFavourite")
        {
            delegate?.showFavourite!(response: responseString )
        }
        else if serviceName.isEqual("AddFavourite")
        {
            delegate?.addFavourite!(response: responseString )
        }
        else if serviceName.isEqual("DeleteFavourite")
        {
            delegate?.deleteFavourite!(response: responseString )
        }
        else if serviceName.isEqual("UploadPicture")
        {
            delegate?.uploadPicture!(response: responseString )
        }
        else if serviceName.isEqual("UploadStory")
        {
            delegate?.uploadStory!(response: responseString )
        }
        else if serviceName.isEqual("UploadMessage")
        {
            delegate?.uploadMessage!(response: responseString )
        }
        else if serviceName.isEqual("DownloadStory")
        {
            delegate?.downloadStory!(response: responseString )
        }
        else if serviceName.isEqual("Post")
        {
            delegate?.post!(response: responseString )
        }
        else if serviceName.isEqual("Post1")
        {
            delegate?.post1!(response: responseString )
        }
        else if serviceName.isEqual("Post2")
        {
            delegate?.post2!(response: responseString )
        }
        else if serviceName.isEqual("Post3")
        {
            delegate?.post3!(response: responseString )
        }
        else if serviceName.isEqual("Post4")
        {
            delegate?.callDelete!(response: responseString )
        }
        else if serviceName.isEqual("Post5")
        {
            delegate?.insertMLSID!(response: responseString )
        }
        else if serviceName.isEqual("Post6")
        {
            delegate?.deleteMLSID!(response: responseString )
        }
        else if responseString.isEqual("Post7")
        {
            delegate?.profileEditing!(response: responseString )
        }
        else if serviceName.isEqual("Post8")
        {
            delegate?.notesUpdating!(response: responseString )
        }
        else if serviceName.isEqual("Post9")
        {
            delegate?.notesDeleting!(response: responseString )
        }
        else if serviceName.isEqual("Post10")
        {
            delegate?.historyLog!(response: responseString )
        }
        else if serviceName.isEqual("Post11")
        {
            delegate?.realtorShow!(response: responseString )
        }
        else if serviceName.isEqual("Post12")
        {
            delegate?.notesgetting!(response: responseString )
        }
        else if serviceName.isEqual("Post13")
        {
            delegate?.updateStatus!(response: responseString )
        }
        else if serviceName.isEqual("Postphp")
        {
            delegate?.postphp!(response: responseString )
        }
    }
    func urlSession(connection: URLSession, canAuthenticateAgainstProtectionSpace protectionSpace: URLProtectionSpace?) -> Bool
    {
        if protectionSpace?.authenticationMethod == NSURLAuthenticationMethodServerTrust
        {
            return true
        }
        else
        {
            if protectionSpace?.authenticationMethod == NSURLAuthenticationMethodHTTPBasic
            {
                return true
            }
        
        }
        return false
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)
    {
        var disposition: URLSession.AuthChallengeDisposition = URLSession.AuthChallengeDisposition.performDefaultHandling
        
         var credential:URLCredential?
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust
        {
            credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            if (credential != nil)
            {
                disposition = URLSession.AuthChallengeDisposition.useCredential
            }
            else
            {
                disposition = URLSession.AuthChallengeDisposition.performDefaultHandling
            }
        }
        else if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodHTTPBasic
        {
            credential = URLCredential(user: "test",
                                           password: "test",
                                           persistence:.none)
        }
        else
        {
            disposition = URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge

        }
        completionHandler(disposition, credential)
        challenge.sender?.continueWithoutCredential(for: challenge)
    }
}



