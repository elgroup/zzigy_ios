//
//  MyCustomClass.swift
//  MySwiftExample
//
//  Created by anupama on 24/01/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit
/////////////////////////////////////////////////////////////////
/////////////////////Importent Note////////////////////////////
//////////// //////////////////////////////////////////////////////
///1. To Remove ARC from spacific class///
///// add -:"-fno-objc-arc" in Compiler Flags///
//// how to go there:-click on target->compile Sources->choose
//// any .m file which you want to remove ARC from file//////
///@end///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
///2. To Remove ARC Related from classes having ARC////
///// add -:"-fobjc-arc-exceptions" in comiler Flags///
///// how to go there:-click on target->compile Sources->choose
//// any .m file which you want to remove ARC from file//////
///@end///////////////////////////////////////////////////////////

var IS_IPHONE6plus = UIScreen.main.bounds.size.height == 736
var IS_IPHONE6 = UIScreen.main.bounds.size.height == 667
var IS_IPHONE5 = UIScreen.main.bounds.size.height == 568
var IS_IPHONE4 = UIScreen.main.bounds.size.height == 480
var CUREENT_SYSTEM_VERSION = UIDevice.current.systemVersion

class MyCustomClass: NSObject
{
    
    //MARK:......................................................................
    //MARK: Save Data in Dictionary after getting JSON response
   static func jsonSerializationDictionary(response: String) -> [String: Any]?
    {
        if let data = response.data(using: .utf8)
        {
            do
            {
               return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    //MARK:......................................................................
    //MARK:Save Data in Array after getting JSON response
    static func jsonSerializationArray(response: String) -> [String]?
    {
        if let data = response.data(using: .utf8)
        {
            do
            {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String]
            }
            catch
            {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    //MARK:......................................................................
    //MARK:Check Json in Array,Dictionary or String after getting JSON response

   static  func checkJsonArrayDictionaryString(response: String) -> Any
    {
        let dataVal = response.data(using: .utf8)
        do {
            if let elim = try JSONSerialization.jsonObject(with: dataVal!, options: []) as? [String: Any]
            {
                print("elim is a dictionary")
                print(elim)
                return elim
               
                // return elim
            } else if let elim = try JSONSerialization.jsonObject(with: dataVal!, options: []) as? [String]
            {
                print("elim is an array")
                 return elim
            }
            else if let elim = try JSONSerialization.jsonObject(with: dataVal!, options: []) as? String
            {
                print("elim is an string")
                print(elim)
                 return elim
            }
           else if (dataVal?.isEmpty)! || (dataVal==nil)
            {
                print(dataVal ?? 0)
                 return dataVal ?? 0
            }

            else
            {
                print("dataVal is not valid JSON data")
            }
        } catch let error as NSError {
            print(error)
        }
       return dataVal ?? 0
    }

    //MARK:......................................................................
    //MARK: Set Any View Color By Passing hexadecimal String.....................
    static func colorWithHexString (hexString:String) -> UIColor
    {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count
        {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        
        return  UIColor.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    //MARK:......................................................................
    //MARK:Url white space replace by %20 in string ..................
    static func whiteSpaceReplaceFromString (insertString:String) -> String
    {
        let urlString = insertString.replacingOccurrences(of:" ", with: "%20")
        return urlString
    }
    
    //MARK:......................................................................
    //MARK:Url Email Validation  .............................................
    static func validateEmail (email:String)-> Bool
    {
        do
        {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: email, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, email.characters.count)) != nil
        }
        catch
        {
            return false
        }
    }
    
    //MARK:......................................................................
    //MARK:Pincode Validation  .............................................
    static func ValidPincode(value: String) -> Bool
    {
        if value.characters.count == 6
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK:......................................................................
    //MARK:Current Password and Confirm Password same Validation  .............................................
    static func newandConfirmPasswordValidation(newpassword: String , confirmPassword : String) -> Bool
    {
        if newpassword == confirmPassword
        {
            return true
        }
        else
        {
            return false
        }
    }
    //MARK:......................................................................
    //MARK:Current Password and Confirm Password Length Validation  .............................................
    static func passwordLengthValidation(password: String , confirmPassword : String , lengthSize:Int ) -> Bool
    {
        if password.characters.count <= lengthSize && confirmPassword.characters.count <= lengthSize
        {
            return true
        }
        else{
            return false
        }
    }
    
    //MARK:......................................................................
    //MARK: Mobile number Validation .............................................
    static func validatePhoneNumber (phoneNumbervalue: String) -> Bool
    {
        let PHONE_REGEX = "[0-9]{10}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        if  phoneTest.evaluate(with: phoneNumbervalue) == true
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK:......................................................................
    //MARK: Mobile number Numeric Validation .............................................
    static func phoneNumberNumericValidation (phoneNumber: String) -> Bool
    {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        if phoneNumber == filtered
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK:......................................................................
    //MARK: Age Validation .............................................
    static func ageValidation (ageValue: String) -> Bool
    {
        let PHONE_REGEX = "[0-9]{2}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        if  phoneTest.evaluate(with: ageValue) == true
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK:......................................................................
    //MARK: Blank String OR Textfield Validation .............................................
    static func blankStringValidation (blankString:String)-> Bool
    {
        let trimmed =  blankString.trimmingCharacters(in: .whitespaces)
        if trimmed.isEmpty
        {
            return true
        }
        else
        {
             return false
        }
    }
    
    //MARK:......................................................................
    //MARK: Password Validation with Minimum range and Maximum range.............................................
    static func passwordValidationWithExpersionAndRange (password:String, minimumLength:Int, maximumLength:Int)-> Bool
    {
        do
        {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z_0-9\\-_,;.:#+*?=!§$%&/()@]+$", options: .caseInsensitive)
            if(regex.firstMatch(in:password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, password.characters.count)) != nil)
            {
                if(password.characters.count>=minimumLength && password.characters.count<=maximumLength)
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            else
            {
                return false
            }
        }
        catch
        {
            return false
        }
    }
    
    //MARK:......................................................................
    //MARK: Dictionary Convert into JsonString............................................
    static func dictionaryConvertIntoJsonString (jsonObjectDictionary: [String: Any]) -> String
    {
        var jsonString :String! = nil
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObjectDictionary, options: .prettyPrinted)
             jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
             return jsonString
        }
        catch
        {
            print(error.localizedDescription)
        }
         return jsonString
    }
    
    //MARK:......................................................................
    //MARK: Dictionary Convert into JsonData............................................
    static func dictionaryConvertIntoJsonData (jsonObjectDictionary: [String: Any]) -> Data
    {
        let jsonData :Data! = nil
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObjectDictionary, options: .prettyPrinted)
            return jsonData
        }
        catch
        {
            print(error.localizedDescription)
        }
        return jsonData
    }
    
    //MARK:......................................................................
    //MARK: Array Convert into JsonData............................................
    static func arrayConvertIntoJsonData (jsonObjectArray: [Any]) -> Data
    {
        var jsonData :Data! = nil
        do
        {
           jsonData = try JSONSerialization.data(withJSONObject: jsonObjectArray, options: .prettyPrinted)
          return jsonData
           
        }
        catch
        {
            print(error.localizedDescription)
        }
        return jsonData
    }
    
    //MARK:......................................................................
    //MARK: Array Convert into JsonString............................................
    static func arrayConvertIntoJsonString (jsonObjectArray: [Any]) -> String
    {
        var jsonString :String! = nil
        do
        {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObjectArray, options: .prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            return jsonString
        }
        catch
        {
            print(error.localizedDescription)
        }
        return jsonString
    }
    //MARK:......................................................................
    //MARK: Different color provide to Single String............................................
    static func createAttributedString(fullString: String, fullStringColor: UIColor, subString: String, subStringColor: UIColor) -> NSMutableAttributedString
    {
        let range = (fullString as NSString).range(of: subString)
        let attributedString = NSMutableAttributedString(string:fullString)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: fullStringColor, range: NSRange(location: 0, length: fullString.characters.count))
        attributedString.addAttribute(NSForegroundColorAttributeName, value: subStringColor, range: range)
        return attributedString
    }
    //MARK:......................................................................
    //MARK: SVProgress Hud Loading Message............................................
   static func SVProgressHUDLoadingMsg(loadingMessage:String)
    {
        //SVProgressHUD.show(loadingMessage)
    }
    
    //MARK:......................................................................
    //MARK: SVProgress Hud SuccessMessage............................................
   static func SVProgressHUDSuccessMsg(successMessage:String, afterDelay:Float)
    {
        //SVProgressHUD.successMsg(successMessage, delayTime: afterDelay)
    }
    
    //MARK:......................................................................
    //MARK: SVProgress Hud Failed Message............................................
   static func SVProgressHUDFailedMsg(successMessage:String, afterDelay:Float)
    {
        //SVProgressHUD.failedMsg(successMessage, delayTime: afterDelay)
    }
    
    //MARK:......................................................................
    //MARK: SVProgress Hud Hide Without Message............................................
   static func SVProgressHUDHideWithoutMsg(afterDelay:Float)
    {
        //SVProgressHUD.hidetimer(time: afterDelay)
    }
}
