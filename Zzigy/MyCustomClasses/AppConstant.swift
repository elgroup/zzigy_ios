//
//  Constant.swift
//  Zzigy
//
//  Created by anupama on 27/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import Foundation
import UIKit

enum UserDefaultsKeys: String {
    case SomeNotification = "aaaaNotification"
    case DeviceToken = "deviceToken"
}


//URLConstants.swift

struct APPURL {
    
    private struct Domains {
        static let Dev = "http://test-dev.cloudapp.net"
        static let UAT = "http://test-UAT.com"
        static let Local = "192.145.1.1"
        static let QA = "testAddress.qa.com"
    }
    
    private  struct Routes {
        static let Api = "/api/mobile"
    }
    
    private  static let Domain = Domains.Dev
    private  static let Route = Routes.Api
    private  static let BaseURL = Domain + Route
    
    static var FacebookLogin: String {
        return BaseURL  + "/auth/facebook"
    }
}

//KeyConstants.swift
struct Key {
    
    static let DeviceType = "iOS"
    struct Beacon{
        static let ONEXUUID = "xxxx-xxxx-xxxx-xxxx"
    }
    
    struct UserDefaults {
        static let k_App_Running_FirstTime = "userRunningAppFirstTime"
    }
    
    struct Headers {
        static let Authorization = "Authorization"
        static let ContentType = "Content-Type"
    }
    struct Google{
        static let placesKey = "some key here"//for photos
        static let serverKey = "some key here"
    }
    
    struct ErrorMessage{
        static let listNotFound = "ERROR_LIST_NOT_FOUND"
        static let validationError = "ERROR_VALIDATION"
    }
}
//FontsConstants.swift
struct FontNames {
    
    static let CircularName = "Circular"
    struct Circular {
        static let CircularBold = "CircularStd-Bold"
        static let CircularRegular = "CircularStd-Book"
    }
}
//ColorConstants.swift
struct AppColor {
    
    private struct Alphas {
        static let Opaque = CGFloat(1)
        static let SemiOpaque = CGFloat(0.8)
        static let SemiTransparent = CGFloat(0.5)
        static let Transparent = CGFloat(0.3)
    }
    
    static let appPrimaryColor =  UIColor.white.withAlphaComponent(Alphas.SemiOpaque)
    static let appSecondaryColor =  UIColor.blue.withAlphaComponent(Alphas.Opaque)
    
    struct TextColors {
        static let Error = AppColor.appSecondaryColor
        static let Success = UIColor(red: 0.1303, green: 0.9915, blue: 0.0233, alpha: Alphas.Opaque)
    }
    struct SegmentedColor {
        static let TextSelected = UIColor(red: 212.0/255, green: 96.0/255, blue: 42/255, alpha: Alphas.Opaque)
        static let TextUnSelected = UIColor(red: 179.0/255, green: 183.0/255, blue: 185.0/255, alpha: Alphas.Opaque)
        static let SelectionIndicatorColor = UIColor(red: 212.0/255, green: 96.0/255, blue: 42.0/255, alpha: Alphas.Opaque)
        static let detailTextUnSelected = UIColor(red: 72.0/255, green: 82.0/255, blue: 116.0/255, alpha: Alphas.Opaque)
        static let detailTextSelected = UIColor(red: 40.0/255, green: 43.0/255, blue: 56.0/255, alpha: Alphas.Opaque)
        static let detailSelectionIndicatorColor = UIColor(red: 61.0/255, green: 183.0/255, blue: 151.0/255, alpha: Alphas.Opaque)

    }
    
    struct TabBarColors{
        static let Selected = UIColor.white
        static let NotSelected = UIColor.black
    }
    
    struct OverlayColor {
        static let SemiTransparentBlack = UIColor.black.withAlphaComponent(Alphas.Transparent)
        static let SemiOpaque = UIColor.black.withAlphaComponent(Alphas.SemiOpaque)
        static let demoOverlay = UIColor.black.withAlphaComponent(0.6)
    }
}
