//
//  WelcomeViewController.swift
//  Zzigy
//
//  Created by anupama on 21/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit
import QuartzCore

class WelcomeViewController: UIViewController {

    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.roundedButton(btn: signUpBtn)
        //self.roundedButton(btn: loginBtn)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func roundedButton(btn:UIButton!){
        let maskPAth1 = UIBezierPath(roundedRect: btn.bounds,
                                     byRoundingCorners: [.topLeft , .topRight , .bottomLeft , .bottomRight],
                                     cornerRadii:CGSize(width:18.0, height:16.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = btn.bounds
        maskLayer1.path = maskPAth1.cgPath
        btn.layer.mask = maskLayer1
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

    // MARK:--- Login Method................................
    @IBAction func clickOnLoginMethod(_ sender: Any)
    {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") else {
            print("View controller not found")
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK:--- Register Method................................
    @IBAction func clickOnSignUpMethod(_ sender: Any)
    {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") else {
            print("View controller not found")
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    

}
