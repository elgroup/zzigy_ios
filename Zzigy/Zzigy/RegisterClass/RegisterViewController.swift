//
//  RegisterViewController.swift
//  Zzigy
//
//  Created by anupama on 21/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet weak var fNameTextField: UITextField!
    @IBOutlet weak var lNameTextField: UITextField!
    @IBOutlet weak var uNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var cell1: UITableViewCell!
    @IBOutlet weak var cell2: UITableViewCell!
    @IBOutlet weak var cell3: UITableViewCell!
    @IBOutlet weak var cell4: UITableViewCell!
    @IBOutlet weak var cell5: UITableViewCell!
    @IBOutlet weak var cell6: UITableViewCell!
    @IBOutlet weak var cell7: UITableViewCell!
    @IBOutlet weak var cell8: UITableViewCell!
    @IBOutlet weak var cell9: UITableViewCell!
    @IBOutlet weak var cell10: UITableViewCell!
    @IBOutlet weak var cell11: UITableViewCell!
    @IBOutlet weak var cell12: UITableViewCell!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var privacyBtn: UIButton!
    @IBOutlet weak var termConditionBtn: UIButton!

    @IBOutlet weak var customTableView: UITableView!
    var cellArray = [AnyObject]()

   let segmentedControl = HMSegmentedControl(items: ["FOODIE", "BUSINESS OWNER"])

    override func viewDidLoad() {
        super.viewDidLoad()
        
        cellArray = [cell1,cell2,cell3,cell4,cell5,cell6,cell7,cell8,cell12,cell9,cell10,cell11]

        customTableView.separatorColor = UIColor.clear
        customTableView.backgroundColor = UIColor.clear

                // Do any additional setup after loading the view.
        customSegementedControl()
        customString()
        //textFieldAtribute()
       
    }
    //MARK:-- Custom String Method...........................

    func customString()  {
        let main_string = "I AGREE TO TERMS & CONDITIONS"
        let string_to_color = "TERMS & CONDITIONS"
        let string = MyCustomClass.createAttributedString(fullString: main_string, fullStringColor: UIColor.init(colorLiteralRed: 145/255, green: 153/255, blue: 161/255, alpha: 1.0), subString: string_to_color, subStringColor:   UIColor.init(colorLiteralRed: 212/255, green: 96/255, blue: 42/255, alpha: 1.0))
        termConditionBtn.setAttributedTitle(string, for: .normal)
        
        let main_string1 = "& PRIVACY POLICY"
        let string_to_color1 = "PRIVACY POLICY"
        let string1 = MyCustomClass.createAttributedString(fullString: main_string1, fullStringColor: UIColor.init(colorLiteralRed: 145/255, green: 153/255, blue: 161/255, alpha: 1.0), subString: string_to_color1, subStringColor:   UIColor.init(colorLiteralRed: 212/255, green: 96/255, blue: 42/255, alpha: 1.0))
        privacyBtn.setAttributedTitle(string1, for: .normal)

    }
    //MARK:-- Custom Segmented Control Method...........................
    func customSegementedControl()
    {
        customView.addSubview(segmentedControl)

        segmentedControl.backgroundColor = UIColor.clear
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.selectionIndicatorPosition = .bottom
        segmentedControl.selectionIndicatorColor =  AppColor.SegmentedColor.SelectionIndicatorColor
        
        segmentedControl.titleTextAttributes = [
            NSForegroundColorAttributeName : AppColor.SegmentedColor.TextUnSelected,
            NSFontAttributeName : UIFont(name: FontNames.Circular.CircularRegular, size: 16)!
        ]
        
        segmentedControl.selectedTitleTextAttributes = [
            NSForegroundColorAttributeName :  AppColor.SegmentedColor.TextSelected,
            NSFontAttributeName : UIFont(name: FontNames.Circular.CircularRegular, size: 16)!
        ]
        segmentedControl.selectionIndicatorHeight = 2
        segmentedControl.setSelectedSegmentIndex(0, animated: true)

        segmentedControl.indexChangedHandler = { index in
            print(index)
            //            print(self.segmentedControl.selectedSegmentIndex)
            //            self.segmentedControl.selectedSegmentIndex = 1
        }
        
        NSLayoutConstraint.activate(
            [segmentedControl.leftAnchor.constraint(equalTo: customView.leftAnchor),
             segmentedControl.heightAnchor.constraint(equalToConstant: 50),
             segmentedControl.rightAnchor.constraint(equalTo: customView.rightAnchor),
             segmentedControl.topAnchor.constraint(equalTo: customView.topAnchor, constant: 0)]
        )

        segmentedControl.indexChangedHandler = { index in
            print(index)
            //            print(self.segmentedControl.selectedSegmentIndex)
            //            self.segmentedControl.selectedSegmentIndex = 1
        }
    }
    //MARK:-- Text Field Attribute Method...........................
    func textFieldAtribute()
    {
        let color = UIColor.init(colorLiteralRed: 62/255, green: 80/255, blue: 119/255, alpha: 1.0)
        fNameTextField.attributedPlaceholder = NSAttributedString(string: "First Name",
                                                                  attributes: [NSForegroundColorAttributeName: color])
        lNameTextField.attributedPlaceholder = NSAttributedString(string: "Last Name",
                                                                  attributes: [NSForegroundColorAttributeName: color])
        uNameTextField.attributedPlaceholder = NSAttributedString(string: "User Name",
                                                                  attributes: [NSForegroundColorAttributeName: color])
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email ID",
                                                                  attributes: [NSForegroundColorAttributeName: color])
        passTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                  attributes: [NSForegroundColorAttributeName: color])
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:- TABLE VIEW DATA SOURCE AND DELEGATE METHOD......
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let  cell = cellArray[indexPath.row] as! UITableViewCell
        
        return cell.frame.size.height
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       let  cell = cellArray[indexPath.row] as! UITableViewCell
        
        return cell;
    }
    // MARK:--- Register Method................................
    @IBAction func clickOnBackMethod(_ sender: Any)
    {
        if let navigationController = navigationController {
            navigationController.popViewController(animated: true)
        }    }
}
