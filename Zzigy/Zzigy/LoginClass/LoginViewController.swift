//
//  LoginViewController.swift
//  Zzigy
//
//  Created by anupama on 21/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController , UITableViewDataSource , UITableViewDelegate{

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var cell1: UITableViewCell!
    @IBOutlet weak var cell2: UITableViewCell!
    @IBOutlet weak var cell3: UITableViewCell!
    @IBOutlet weak var cell4: UITableViewCell!
    @IBOutlet weak var cell5: UITableViewCell!
    @IBOutlet weak var cell6: UITableViewCell!
    @IBOutlet weak var customTableView: UITableView!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!

    let segmentedControl = HMSegmentedControl(items: ["FOODIE", "BUSINESS OWNER"])
     var cellArray = [AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()

        cellArray = [cell1,cell2,cell3,cell4,cell5,cell6]
        
        customTableView.separatorColor = UIColor.clear
        customTableView.backgroundColor = UIColor.clear
        
        customSegementedControl()
        textFieldAtribute()
        customString()
        // Do any additional setup after loading the view.
       
        
    }
    //MARK:-- Custom String Method...........................
    func customString()
    {
        let main_string = "NOT A USER? SIGN UP"
        let string_to_color = "SIGN UP"
        let string = MyCustomClass.createAttributedString(fullString: main_string, fullStringColor: UIColor.init(colorLiteralRed: 145/255, green: 153/255, blue: 161/255, alpha: 1.0), subString: string_to_color, subStringColor:   UIColor.init(colorLiteralRed: 212/255, green: 96/255, blue: 42/255, alpha: 1.0))
        signUpBtn.setAttributedTitle(string, for: .normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-- Custom Segmented Control Method...........................
    func customSegementedControl()
    {
        customView.addSubview(segmentedControl)
        
        segmentedControl.backgroundColor = UIColor.clear
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.selectionIndicatorPosition = .bottom
        segmentedControl.selectionIndicatorColor =  AppColor.SegmentedColor.SelectionIndicatorColor
      
        segmentedControl.titleTextAttributes = [
            NSForegroundColorAttributeName :AppColor.SegmentedColor.TextUnSelected,
            NSFontAttributeName : UIFont(name: FontNames.Circular.CircularRegular, size: 16)!
        ]
        
        segmentedControl.selectedTitleTextAttributes = [
            NSForegroundColorAttributeName :  AppColor.SegmentedColor.TextSelected,
            NSFontAttributeName : UIFont(name: FontNames.Circular.CircularRegular, size: 16)!
        ]
        segmentedControl.selectionIndicatorHeight = 2
        segmentedControl.setSelectedSegmentIndex(0, animated: true)
        segmentedControl.indexChangedHandler = { index in
            print(index)
            //            print(self.segmentedControl.selectedSegmentIndex)
            //            self.segmentedControl.selectedSegmentIndex = 1
        }
        
        NSLayoutConstraint.activate(
            [segmentedControl.leftAnchor.constraint(equalTo: customView.leftAnchor),
             segmentedControl.heightAnchor.constraint(equalToConstant: 50),
             segmentedControl.rightAnchor.constraint(equalTo: customView.rightAnchor),
             segmentedControl.topAnchor.constraint(equalTo: customView.topAnchor, constant: 0)]
        )
        
        segmentedControl.indexChangedHandler = { index in
            print(index)
            //            print(self.segmentedControl.selectedSegmentIndex)
            //            self.segmentedControl.selectedSegmentIndex = 1
        }
    }
    //MARK:-- Text Field Attribute Method...........................
    func textFieldAtribute()
    {
        let color = UIColor.init(colorLiteralRed: 62/255, green: 80/255, blue: 119/255, alpha: 1.0)
        
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email ID",
                                                                  attributes: [NSForegroundColorAttributeName: color])
        passTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                 attributes: [NSForegroundColorAttributeName: color])
    }
    // MARK:- TABLE VIEW DATA SOURCE AND DELEGATE METHOD......
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let  cell = cellArray[indexPath.row] as! UITableViewCell
        
        return cell.frame.size.height
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let  cell = cellArray[indexPath.row] as! UITableViewCell
        
        return cell;
    }

    // MARK:--- Login Method................................
    @IBAction func clickOnLoginMethod(_ sender: Any)
    {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") else {
            print("View controller not found")
            return
        }
       navigationController?.pushViewController(vc, animated: true)
        //guard let vc = storyboard?.instantiateViewController(withIdentifier: "CategoryViewController") else {
           // print("View controller not found")
           // return
        //}
       // navigationController?.pushViewController(vc, animated: true)
    }
    // MARK:--- Register Method................................
    @IBAction func clickOnSignUpMethod(_ sender: Any)
    {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") else {
            print("View controller not found")
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK:--- Forgot Password Method................................
    @IBAction func clickOnForgotPasswordMethod(_ sender: Any)
    {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotViewController") else {
            print("View controller not found")
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK:--- FaceBook Method................................
    @IBAction func clickOnFaceBookMethod(_ sender: Any)
    {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotViewController") else {
            print("View controller not found")
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
   

}
