//
//  HomeViewController.swift
//  Zzigy
//
//  Created by anupama on 21/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController , UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var homeTableView: UITableView!
    var gridViewCell:HomeTableViewCell! = nil
    @IBOutlet weak var HomeTableViewCell: UITableViewCell!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        homeTableView.separatorColor = UIColor.clear
        homeTableView.backgroundColor = UIColor.clear
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- TABLE VIEW DATA SOURCE AND DELEGATE METHOD......
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let customCell: HomeTableViewCell! = homeTableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell") as? HomeTableViewCell
        customCell.selectionBtn.addTarget(self, action: #selector(clickOnSelectionBtnmethod), for: .touchUpInside)
        return customCell
    }

    @IBAction func clickOnSelectionBtnmethod(_ sender: Any)
    {
        let button =  sender as! UIButton
        let pointInSuperview = (sender as AnyObject).convert(button.center, to: homeTableView)
        let indexPath = homeTableView.indexPathForRow(at: pointInSuperview)
        print(indexPath?.row ?? 0)
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "RestaurantDetailViewController") else {
            print("View controller not found")
            return
        }
        navigationController?.pushViewController(vc, animated: true)


    }


}
