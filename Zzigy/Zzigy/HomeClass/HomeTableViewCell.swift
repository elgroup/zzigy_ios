//
//  HomeTableViewCell.swift
//  Zzigy
//
//  Created by anupama on 24/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var selectionBtn: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        foodImageView.layer.cornerRadius = foodImageView.frame.size.width/2
        foodImageView.layer.borderColor = UIColor.init(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 1.0).cgColor
        foodImageView.clipsToBounds = true
        
        roundedButton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func roundedButton(){
        let maskPAth1 = UIBezierPath(roundedRect: distanceLabel.bounds,
                                     byRoundingCorners: [.topLeft , .topRight , .bottomLeft , .bottomRight],
                                     cornerRadii:CGSize(width:15.0, height:15.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = distanceLabel.bounds
        maskLayer1.path = maskPAth1.cgPath
        distanceLabel.layer.mask = maskLayer1
    }
}
