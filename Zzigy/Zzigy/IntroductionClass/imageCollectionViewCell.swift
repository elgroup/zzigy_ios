//
//  imageCollectionViewCell.swift
//  Zzigy
//
//  Created by anupama on 21/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class imageCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var photoImageView: UIImageView!

    func configureCell(photoName:UIImage){
        photoImageView?.image = photoName
    }
   
    
}
