//
//  IntroductionViewController.swift
//  Zzigy
//
//  Created by anupama on 21/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit
import QuartzCore

class IntroductionViewController: UIViewController,
UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var startedBtn: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    var photoImageArray = [AnyObject]()
    var gridViewCell:imageCollectionViewCell! = nil
     var count:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        ///self.roundedButton()
        
        photoImageArray = [UIImage(named: "intro1.png")!,UIImage(named: "intro2.png")!,UIImage(named: "intro3.png")!]
        
        imageCollectionView.reloadData()
        imageCollectionView.setContentOffset(imageCollectionView.contentOffset, animated:false)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func roundedButton(){
        let maskPAth1 = UIBezierPath(roundedRect: startedBtn.bounds,
                                     byRoundingCorners: [.topLeft , .topRight , .bottomLeft , .bottomRight],
                                     cornerRadii:CGSize(width:15.0, height:15.0))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = startedBtn.bounds
        maskLayer1.path = maskPAth1.cgPath
        startedBtn.layer.mask = maskLayer1
    }
    // MARK: - Started Method.........................
    @IBAction func ClickOnStartedBtnMethod(_ sender: Any)
    {
        if let startedBtn = sender as? UIButton {
            if startedBtn.isSelected
            {
                // set selected
                print("hbewfhbrewfjf")
                startedBtn.isSelected = true
            }
            else
            {
                // set deselected
                startedBtn.isSelected = false
               count = count+1
                print(count,photoImageArray.count)
                if count > photoImageArray.count-1
                {
                    guard let vc = storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") else {
                    print("View controller not found")
                    return
                    }
                    navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    self.setScrollCollectionViewindex(oldRow: count)
                }
                
            }
        }
        
    }
    
    // MARK: - Collection View Data Source And Delegate Methods.........................

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoImageArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        var cell:UICollectionViewCell! = nil
        gridViewCell = imageCollectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionViewCell", for: indexPath as IndexPath) as! imageCollectionViewCell
        
        let image:UIImage! =  photoImageArray[indexPath.row] as! UIImage
        gridViewCell.configureCell(photoName: image)
        cell = gridViewCell
        return cell
    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let picDimension = self.view.frame.size.width
        let picDimension1 = self.view.frame.size.height

        return CGSize(width: picDimension, height: picDimension1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print(imageCollectionView.frame.size.width)
        let pageWidth:CGFloat = imageCollectionView.frame.size.width
        let fractionalPage:Float = Float(imageCollectionView.contentOffset.x/pageWidth)
        let page = lround(Double(fractionalPage))
        pageControl.currentPage = page
        print(page)
    }
    
    // MARK: - Set scroll collection view index Methods.........................

    func setScrollCollectionViewindex(oldRow:NSInteger)
    {
        pageControl.currentPage = oldRow

        let newIndexpath = NSIndexPath.init(row: oldRow, section: 0)
        imageCollectionView.scrollToItem(at: newIndexpath as IndexPath, at:.centeredHorizontally, animated: true)
    }
 }
