//
//  CategoryViewController.swift
//  Zzigy
//
//  Created by anupama on 21/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate{
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    var gridViewCell:CategoryCollectionViewCell! = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Collection View Data Source And Delegate Methods.........................
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        gridViewCell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath as IndexPath) as! CategoryCollectionViewCell
        
        //let image:UIImage! =  photoImageArray[indexPath.row] as! UIImage
        //gridViewCell.configureCell(photoName: image)
        return gridViewCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        let picDimension = self.view.frame.size.width
//        //let picDimension1 = self.view.frame.size.height
//        
//        return CGSize(width: picDimension, height: 150)
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsetsMake(0, 0, 0, 0)
//    }


}
