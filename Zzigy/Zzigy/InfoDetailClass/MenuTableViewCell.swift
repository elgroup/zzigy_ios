//
//  MenuTableViewCell.swift
//  Zzigy
//
//  Created by anupama on 27/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        foodImageView.layer.cornerRadius = foodImageView.frame.size.width/2
        foodImageView.layer.borderColor = UIColor.init(colorLiteralRed: 255/255, green: 255/255, blue: 255/255, alpha: 1.0).cgColor
        foodImageView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
