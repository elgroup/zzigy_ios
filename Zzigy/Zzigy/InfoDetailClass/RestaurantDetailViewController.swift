//
//  RestaurantDetailViewController.swift
//  Zzigy
//
//  Created by anupama on 27/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class RestaurantDetailViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource , UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var customView: UIView!
    let segmentedControl = HMSegmentedControl(items: ["ABOUT", "MENU","GALLERY"])
    @IBOutlet weak var cell1: UITableViewCell!
    @IBOutlet weak var cell2: UITableViewCell!
    @IBOutlet weak var cell3: UITableViewCell!
    @IBOutlet weak var cell4: UITableViewCell!
    @IBOutlet weak var aboutTableView: UITableView!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    var gridViewCell:GalleryCollectionViewCell! = nil

    var cellArray = [AnyObject]()

    @IBOutlet weak var restaurantAddressLabel: UILabel!
    @IBOutlet weak var restaurantTypeLabel: UILabel!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var aboutTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        cellArray = [cell1,cell2,cell3,cell4]
        aboutTableView.backgroundColor = UIColor.clear
        aboutTableView.separatorColor = UIColor.clear
        
        menuTableView.backgroundColor = UIColor.clear
        menuTableView.separatorColor = UIColor.clear
        menuTableView.delegate = self
        menuTableView.dataSource = self
        
        galleryCollectionView.delegate = self
        galleryCollectionView.dataSource = self
        galleryCollectionView.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
        customSegementedControl()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:-- Custom Segmented Control Method...........................
    func customSegementedControl()
    {
        customView.addSubview(segmentedControl)
        
        segmentedControl.backgroundColor = UIColor.clear
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.selectionIndicatorPosition = .bottom
        segmentedControl.selectionIndicatorColor =  UIColor.init(colorLiteralRed: 61/255, green: 183/255, blue: 151/255, alpha: 1.0)
        
        segmentedControl.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.init(colorLiteralRed: 72/255, green: 82/255, blue: 116/255, alpha: 1.0),
            NSFontAttributeName : UIFont(name: FontNames.Circular.CircularRegular, size: 16)!
        ]
        
        segmentedControl.selectedTitleTextAttributes = [
            NSForegroundColorAttributeName :  UIColor.init(colorLiteralRed: 40/255, green: 43/255, blue: 56/255, alpha: 1.0),
            NSFontAttributeName : UIFont(name: FontNames.Circular.CircularRegular, size: 16)!
        ]
        segmentedControl.selectionIndicatorHeight = 3
        segmentedControl.setSelectedSegmentIndex(0, animated: true)
        segmentedControl.indexChangedHandler = { index in
            print(index)
            //            print(self.segmentedControl.selectedSegmentIndex)
            //            self.segmentedControl.selectedSegmentIndex = 1
        }
        
        NSLayoutConstraint.activate(
            [segmentedControl.leftAnchor.constraint(equalTo: customView.leftAnchor),
             segmentedControl.heightAnchor.constraint(equalToConstant: 45),
             segmentedControl.rightAnchor.constraint(equalTo: customView.rightAnchor),
             segmentedControl.topAnchor.constraint(equalTo: customView.topAnchor, constant: 0)]
        )
        self.aboutTableView.isHidden = false
        self.menuTableView.isHidden = true
        self.galleryCollectionView.isHidden = true

        segmentedControl.indexChangedHandler = { index in
            print(index)
            
            if index == 0
            {
                self.aboutTableView.isHidden = false
                self.menuTableView.isHidden = true
                self.galleryCollectionView.isHidden = true

                self.aboutTableView .reloadData()
            }
            else if index == 1
            {
                self.menuTableView.isHidden = false
                self.aboutTableView.isHidden = true
                self.galleryCollectionView.isHidden = true

                self.menuTableView.reloadData()
            }
            else if index == 2
            {
                self.galleryCollectionView.isHidden = false
                self.menuTableView.isHidden = true
                self.aboutTableView.isHidden = true
                self.galleryCollectionView.reloadData()
            }
            //            print(self.segmentedControl.selectedSegmentIndex)
            //            self.segmentedControl.selectedSegmentIndex = 1
        }
    }
    // MARK:- TABLE VIEW DATA SOURCE AND DELEGATE METHOD......
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == aboutTableView
        {
            return cellArray.count

        }
        else
        {
            return 10
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == aboutTableView
        {
            let  cell = cellArray[indexPath.row] as! UITableViewCell
            return cell.frame.size.height
        }
        else
        {
            return 120
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == aboutTableView
        {
            let  cell = cellArray[indexPath.row] as! UITableViewCell
            return cell;
        }
        else
        {
            let customCell: MenuTableViewCell! = menuTableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as? MenuTableViewCell
            return customCell
        }
    }
    // MARK: - Collection View Data Source And Delegate Methods.........................
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
         gridViewCell = galleryCollectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath as IndexPath) as! GalleryCollectionViewCell
        
        //let image:UIImage! =  photoImageArray[indexPath.row] as! UIImage
        //gridViewCell.configureCell(photoName: image)
        return gridViewCell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let picDimension = self.view.frame.size.width
        //let picDimension1 = self.view.frame.size.height
        
        return CGSize(width: picDimension, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    // MARK: - Call Btn Methods.........................
    @IBAction func clickOnCallMethodBtn(_ sender: Any) {
    }
    
    // MARK: - Location Btn Methods.........................
    @IBAction func clickOnLocationMethodBtn(_ sender: Any)
    {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "RestaurantMapViewController") else {
            print("View controller not found")
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Visit Website Btn Methods.........................
    @IBAction func clickOnWebsiteBtn(_ sender: Any) {
    }
    
    // MARK: - Back Btn Methods.........................
    @IBAction func backBtnMethod(_ sender: Any)
    {
        if let navigationController = navigationController {
            navigationController.popViewController(animated: true)
        }
    }
    
    // MARK: - Share Btn Methods.........................
    @IBAction func shareBtnMethod(_ sender: Any) {
    }
}
