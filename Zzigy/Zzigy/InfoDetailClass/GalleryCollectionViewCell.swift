//
//  GalleryCollectionViewCell.swift
//  Zzigy
//
//  Created by anupama on 27/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!

}
