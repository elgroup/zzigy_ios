//
//  ForgotViewController.swift
//  Zzigy
//
//  Created by anupama on 21/03/17.
//  Copyright © 2017 Imac6. All rights reserved.
//

import UIKit

class ForgotViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var forgotBtn: UIButton!
    @IBOutlet weak var customView: UIView!

    let segmentedControl = HMSegmentedControl(items: ["FOODIE", "BUSINESS OWNER"])

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        customSegementedControl()
        textFieldAtribute()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-- Custom Segmented Control Method...........................
    func customSegementedControl()
    {
        customView.addSubview(segmentedControl)

        segmentedControl.backgroundColor = UIColor.clear
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.selectionIndicatorPosition = .bottom
        segmentedControl.selectionIndicatorColor =  UIColor.init(colorLiteralRed: 212/255, green: 96/255, blue: 42/255, alpha: 1.0)
        
        
        segmentedControl.titleTextAttributes = [
            NSForegroundColorAttributeName : UIColor.init(colorLiteralRed: 179.0/255, green: 183.0/255, blue: 185.0/255, alpha: 1.0),
            NSFontAttributeName : UIFont(name: FontNames.Circular.CircularRegular, size: 16)!
        ]
        
        segmentedControl.selectedTitleTextAttributes = [
            NSForegroundColorAttributeName :  UIColor.init(colorLiteralRed: 212/255, green: 96/255, blue: 42/255, alpha: 1.0),
            NSFontAttributeName : UIFont(name: FontNames.Circular.CircularRegular, size: 16)!
        ]
        segmentedControl.selectionIndicatorHeight = 2
        segmentedControl.setSelectedSegmentIndex(0, animated: true)

        segmentedControl.indexChangedHandler = { index in
            print(index)
            //            print(self.segmentedControl.selectedSegmentIndex)
            //            self.segmentedControl.selectedSegmentIndex = 1
        }
        
        NSLayoutConstraint.activate(
            [segmentedControl.leftAnchor.constraint(equalTo: customView.leftAnchor),
             segmentedControl.heightAnchor.constraint(equalToConstant: 50),
             segmentedControl.rightAnchor.constraint(equalTo: customView.rightAnchor),
             segmentedControl.topAnchor.constraint(equalTo: customView.topAnchor, constant: 0)]
        )
        
        segmentedControl.indexChangedHandler = { index in
            print(index)
            //            print(self.segmentedControl.selectedSegmentIndex)
            //            self.segmentedControl.selectedSegmentIndex = 1
        }
    }
    //MARK:-- Text Field Attribute Method...........................
    func textFieldAtribute()
    {
        let color = UIColor.init(colorLiteralRed: 62/255, green: 80/255, blue: 119/255, alpha: 1.0)
        
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email ID",
                                                                  attributes: [NSForegroundColorAttributeName: color])
        
    }

    // MARK:--- Register Method................................
    @IBAction func clickOnBackMethod(_ sender: Any)
    {
        if let navigationController = navigationController {
            navigationController.popViewController(animated: true)
        }
    }

}
